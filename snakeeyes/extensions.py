from flask_debugtoolbar import DebugToolbarExtension
from flask_mail import Mail
from flask_wtf import CSRFProtect
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

debug_toolbar = DebugToolbarExtension() #1o5
mail = Mail()#2o5
csrf = CSRFProtect()#3o5
db = SQLAlchemy()#4o5
login_manager = LoginManager()#5o5
