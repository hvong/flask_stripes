#udemy, flasks
section 12

email: halvong@yahoo.com
passwd: 12345678
username: halvong

#-----------------------------------
#path to source
	cd /home/hal/Documents/softwares/python_eclipse/flask_workspace/flask-app12b; source ~/venv3/bin/activate
	cd /home/hal/Documents/softwares/python_eclipse/flask_workspace/flask-app12b

#bitbucket code
	https://hvong@bitbucket.org/hvong/flask_stripes.git

#run without docker
1. check postgresql status 
	postgres/postgres, db: snakeeyes 
	sudo systemctl status postgresql

	#port 5432

2. start up redis 
	cd /home/hal/Documents/softwares/redis-4.0.2; ./src/redis-server

	#cli
	cd /home/hal/Documents/softwares/redis-4.0.2; ./src/redis-cli

	#port 6379

3. start webserver 
	cd /home/hal/Documents/softwares/python_eclipse/flask_workspace/flask-app12b; source ~/venv3/bin/activate
	gunicorn -b 0.0.0.0:8000 --access-logfile - "snakeeyes.app:create_app()" 
	
4. start Celery 
	cd /home/hal/Documents/softwares/python_eclipse/flask_workspace/flask-app12b; celery worker -l info -A snakeeyes.blueprints.contact.tasks

5. start up redis-commander 
	anywhere, redis-commander

	
#Stop
#stop gunicorn
	pkill gunicorn

#stop Celery
	pkill -9 -f 'celery worker'
		or
	ps auxww | grep 'celery worker' | awk '{print $2}' | xargs kill -9

#stop redis
	1. cd /home/hal/Documents/softwares/redis-4.0.2; ./src/redis-cli
	2. help @server 
	3. SHUTDOWN save

#view gunicorn processes 
	ps ax|grep gunicorn

#new project
#-----------------------------------
#Celery config
config/settings.py 
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
db_uri = 'postgresql://postgres:postgres@localhost:5432/snakeeyes'

#requirements.txt
#check requirements.txt
WTForms-Components==0.9.7
Flask-Login==0.3.2

#Dockerfile
change FROM python:3.6-alpine in Dockerfile 

#----
#custom project
1. In the root folder, 
 	a. copy appropriate setup.py		
 	b. ensure setup.py name correlates to project name 
	c. ~/venv3/bin/activate 
	d. pip install --editable .  

#-----------------------------------

#code path
cd /home/hal/Documents/codes/flask/flask-saas 

#-----------------------------------
#starts app

#create image of app, build once when code changes
docker-compose up --build

	1. go to localhost:8000	

#starts app
docker-compose up

#stops app
docker-compose stop

#starts with Click
docker-compose up --build
docker-compose exec website snakeeyes
docker-compose exec website snakeeyes flake8

#-----------------------------------
#docker initialized
docker-compose exec website snakeeyes db init
docker-compose exec website snakeeyes db seed

#code path
cd /home/hal/Documents/codes/python/bsawf-course

#create image of app, build once
docker-compose up --build

#show all images
docker images

#show all containers for this project
docker-compose ps


#starts app
docker-compose up

#stops app
docker-compose stop

#show container pertains to this project
docker ps

#removes containers of this project 
docker-compose rm -f

#removes dangling images
docker rmi -f $(docker images -qf dangling=true)

#installation of dependencies
#install docker-compose
https://docs.docker.com/compose/install/#install-compose
