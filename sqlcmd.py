from sqlalchemy_utils import database_exists, create_database
from snakeeyes.app import create_app
from snakeeyes.extensions import db
from snakeeyes.blueprints.user.models import User

# Create an app context for the database connection.

def init(with_testdb = True):
    """
    Initialize the database.

    :param with_testdb: Create a test database
    :return: None
    """
    db.drop_all()
    db.create_all()

    if with_testdb:
        db_uri = '{0}_test'.format(app.config['SQLALCHEMY_DATABASE_URI'])

        if not database_exists(db_uri):
            create_database(db_uri)

    return None


def seed():
    """
    Seed the database with an initial user.

    :return: User instance
    """
    if User.find_by_identity(app.config['SEED_ADMIN_EMAIL']) is not None:
        return None

    params = {
        'role': 'admin',
        'email': app.config['SEED_ADMIN_EMAIL'],
        'password': app.config['SEED_ADMIN_PASSWORD']
    }

    return User(**params).save()


def reset(ctx, with_testdb):
    """
    Init and seed automatically.

    :param with_testdb: Create a test database
    :return: None
    """
    ctx.invoke(init, with_testdb=with_testdb)
    ctx.invoke(seed)

    return None

if __name__ == "__main__":
    print ("create app object")
    app = create_app()
    print ("Initialized db")
    db.app = app
    #db.drop_all()
    #db.create_all()
    #seed()
